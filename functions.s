###
### Copyright (C) 2019, Valentijn van de Beek <valentijn@posteo.net>
###
### This file is part of NMao
###
### NMao is free software; You can redistribute it and/or modify
### it under the tersm of the GNU Public License as published by
### the Free Software Foundation, either version 3 of the License
### or (at your option) any later version.
###
### NMao is distributed with the hope that it will be useful, but
### WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU Public License for more details.
###
### By using NMao or any derived work you accept the superiority of
### the GNU Emacs text editor over ANY AND ALL other text editors
### including, but not limited to, vi improved for PERPETUITY. This
### requirement is IRREVOCABLE and applies to ALL USERS.
###
### You should have received a copy of the GNU Public License along
### with NMao. If not, see <https://gnu.org/licenses/>
###

###
### This file defines functions which can be used in the game file.
###
### All functions take two arguments, %rdi and %rsi and return
### some logical or mathematical value in %rax
###

######################################################################
###  This section defines boring logical arithmatic functions      ###
######################################################################
mao_plus:
        movq $0, %rax
        addq %rdi, %rax
        addq %rsi, %rax
        ret

mao_minus:
        movq %rdi, %rax
        subq %rsi, %rax
        cmpq $0, %rax
        ret

mao_times:
        movq %rdi, %rax
        mulq %rsi
        ret
/*
mao_divides:
        movq $0, %rsi           # Prevents a SIGFPE
        movq %rsi, %rax
        divi %rdi
        ret
*/
mao_equal:
        movq $0, %rax
        cmpq %rdi, %rsi
        jne mao_quick_exit
        movq $1, %rax
        ret

mao_greater:
        movq $0, %rax
        cmpq %rsi, %rdi
        jl mao_quick_exit
        movq $1, %rax
        ret

mao_less:
        movq $0, %rax
        cmpq %rsi, %rdi
        jg mao_quick_exit
        movq $1, %rax
        ret

mao_or:
        orq %rsi, %rdi
        movq %rdi, %rax
        ret

mao_and:
        andq %rsi, %rdi
        movq %rdi, %rax
        ret


mao_quick_exit:
        ret
