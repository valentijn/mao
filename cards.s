###
### Copyright (C) 2019, Valentijn van de Beek <valentijn@posteo.net>
###
### This file is part of NMao
###
### NMao is free software; You can redistribute it and/or modify
### it under the tersm of the GNU Public License as published by
### the Free Software Foundation, either version 3 of the License
### or (at your option) any later version.
###
### NMao is distributed with the hope that it will be useful, but
### WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU Public License for more details.
###
### By using NMao or any derived work you accept the superiority of
### the GNU Emacs text editor over ANY AND ALL other text editors
### including, but not limited to, vi improved for PERPETUITY. This
### requirement is IRREVOCABLE and applies to ALL USERS.
###
### You should have received a copy of the GNU Public License along
### with NMao. If not, see <https://gnu.org/licenses/>
###

###
### This file defines the internal structure of the cards for the Mao game
###
.include "constants.s"
.global current_card, previous_card, thrown_cards, users_hand
.global dealers_hand, card_play, card_get, card_set, card2str,
.global get_property, initialize_cards, print_users_hand, card2data

.text
ten:    .asciz "10"
jack:   .asciz "Jack"
queen:  .asciz "Queen"
king:   .asciz "King"
ace:    .asciz "Ace"
spades: .asciz "Spades"
hearts: .asciz "Hearts"
clubs:  .asciz "Clubs"
diamonds:       .asciz "Diamonds"
black:  .asciz "Black"
red:    .asciz "Red"
card_print_str: .asciz "%s of %s"
description:    .asciz "%d %d %d"

###
### Returns a property of some special type
###
### Arguments:
###    %rdi - the location of the special value
###    %rsi - the property you want to return
###
### Returns:
###    the property that you want to return or -1 if it can't find it.
###
special_values_table:
        .quad current_card
        .quad previous_card
        .quad users_hand
        .quad dealers_hand
        .quad thrown_cards

get_property:
        ## They are store in quads so we need to multiply by 8
        shl $3, %rdi

        ## We add the offset to our table
        addq $special_values_table, %rdi

        ## We move the address into %rdi
        movq (%rdi), %rdi

        ## If it less then three we can just move it
        cmpq $3, %rsi
        je get_length
        jg get_cards

        movzbq (%rdi, %rsi, 1), %rax
        ret

get_length:
        movzbl (%rdi), %rax
        ret

get_cards:
        incq %rsi
        movq %rsi, %rax
        ret

###
### Get a random card
###
### Returns:
###   0 if it found a card and -1 if the dealer's hand is empty
###
card_get:
        movb (dealers_hand), %dil
        test %dil, %dil
        jne card_get_generate

        ## If the dealer's hand is empty we want to return
        ## an exit code indicating we failed
        movq $-1, %rax
        ret

card_get_generate:
        movq $0, %rdx           # If we don't set to this 0 we get a SIGFPE
        movq $51, %r11          # We define our range
        rdrand %ax              # Generate a random number
        divw %r11w              # Divide the number by 52

        ## dx is the remainder in the right range.
        movzwq %dx, %rdi
        call card_add_user_card

        ## We want to check if the card isn't already set and else try again
        cmpq $0, %rax
        jne card_get_generate

        ## We found a card so we are done now.
        movq $0, %rax
        ret

###
### Sets a card to a specific set of values.
###
### Arguments:
###   %rdi - the location of the card
###   %rsi - the colour of the card
###   %rdx - the suit of the card
###   %rcx - the value of the card
###
card_set:
        movb %sil, (%rdi)
        incq %rdi
        movb %dl, (%rdi)
        incq %rdi
        movb %cl, (%rdi)
        ret

###
### Adds a card to users hand
###
### Arguments:
###     %rdi - an integer with the location of the card in the array
###
### Returns:
###   0 if the operation was successful or -1 if the card was already set
###
card_add_user_card:
        ## First we find the card and see if it has already been set
        shl $2, %rdi        # A logical shift of two is equal to multiplying by 4
        addq $1, %rdi       # Offset for the length bit

        ## Check the length bit
        movb dealers_hand(,%rdi), %bl

        ## Jump to the end
        cmpb $1, %bl
        je card_add_user_card_taken

        ## The first item in the users hand is the amount of cards which
        ## we need to grab first to know where we are in the array
        ## We move zero extended the length to prevent cruft in %rsi
        movzbq (users_hand), %rsi

        ## We quickly increment and save the length of the array
        movq %rsi, %rbx
        incq %rbx
        movb %bl, (users_hand)

        ## We move get the right offset in the array
        shlq $2, %rsi

        ## We move up by one to take the length in account
        incq %rsi

        ## Decrement the current cards in the dealers hand
        movb (dealers_hand), %bl
        subb  $1, %bl
        movb %bl, (dealers_hand)

        ## Set the disabled bit
        movb $1, dealers_hand(,%rdi)
        incq %rdi

        movb dealers_hand(,%rdi), %bl
        movb %bl, users_hand(,%rsi)
        incq %rdi
        incq %rsi

        movb dealers_hand(,%rdi), %bl
        movb %bl, users_hand(,%rsi)
        incq %rdi
        incq %rsi

        movb dealers_hand(,%rdi), %bl
        movb %bl, users_hand(,%rsi)
        movq $0, %rax
        ret

card_add_user_card_taken:
        movq $-1, %rax
        ret

###
###
### Copies a card into another card
###
### Arguments:
###   %rdi - the location of the first card
###   %rsi - the location of the second card
###
card_copy:
        .rep 3
        movb (%rdi), %bl
        movb %bl, (%rsi)
        incq %rdi; incq %rsi
        .endr

        ret

###
### Play a card
###
### Arguments:
###   %rdi - colour of the card
###   %rsi - the suit of the card
###   %rdx - the value of the card
###
card_play:
        pushq %rbp
        movq %rsp, %rbp

        ## We push our values onto the stack and we'll use them from there for ease
        pushq %rdi
        pushq %rsi
        pushq %rdx

        call card_remove_user_card

        cmpq $-1, %rax
        je card_play_end

        ## First copy the previous card into the current card
        movq $current_card, %rdi
        movq $previous_card, %rsi
        call card_copy

        ## Set the current card to the new value
        movq $current_card, %rdi
        movq -8(%rbp), %rsi
        movq -16(%rbp), %rdx
        movq -24(%rbp), %rcx
        call card_set

        ## We add one to the length of thrown cards and add it to the array
        movq $current_card, %rdi
        movq $thrown_cards, %rsi
        addq %r12, %rsi
        call card_copy
        addq $4, %r12

card_play_end:
        ## We skip popping them to avoid unneccessary stack movement
        movq %rbp, %rsp
        popq %rbp
        ret


###
### Removes a card from a user's hand places it into the dealer's hand
###
### Arguments:
###    %rdi - the colour of the card
###    %rsi - the suit of the card
###    %rdx - the value of the card
###
### Returns:
###    zero if the procedure or -1 if it fails.
###
card_remove_user_card:
        ## Setup the stack frame
        pushq %rbp
        movq %rsp, %rbp

        ## God, I wish I had a linked list, this is pain.
        movq $users_hand, %rcx
        movzbl (%rcx), %r8 # The amount of cards in the user's hand

        ## Basically we take the number, decrement it and put it back
        movq %r8, %r9
        decb %r9b
        movb %r9b, (%rcx)

        ## We go to the start of the card and set the return value
        incq %rcx
        movq $0, %rax

card_remove_user_card_loop:
        ## We set a temporary value to store the array, so we don't
        ## lose where the beginning of this card is
        movq %rcx, %r9

        ## We compare the colour, suit and value bit by bit
        cmpb %dil, (%r9)
        jne card_remove_user_card_loop_end
        incq %r9
        cmpb %sil, (%r9)
        jne card_remove_user_card_loop_end
        incq %r9
        cmpb %dl, (%r9)
        jne card_remove_user_card_loop_end

        ## We skip two values to deal with padding bit
        addq $2, %r9

        ## We now need to shift all the cards so this card is removed
        ## no matter where it is in the array
        jmp card_remove_user_card_remove

card_remove_user_card_loop_end:
        ## This ain't it chief.
        addq $4, %rcx

        ## We want to loop for the entire length of the array
        decq %r8
        test %r8, %r8
        jnz card_remove_user_card_loop

        ## If we reached the end of the array without finding our card
        ## we need to set our return variable to -1 to indicate we failed.
        movq $-1, %rax
        jmp card_remove_user_card_set_bit

card_remove_user_card_remove:
        ## Bunch of boring stuff which moves all cards above the
        ## removed card down by one. We know %r9 is at the next
        ## card and that %rcx points to the location where the
        ## removed card was.
        movb (%r9), %r10b
        movb %r10b, (%rcx)
        incq %rcx
        incq %r9

        movb (%r9), %r10b
        movb %r10b, (%rcx)
        incq %rcx
        incq %r9

        movb (%r9), %r10b
        movb %r10b, (%rcx)
        incq %rcx
        incq %r9

        decq %r8
        test %r8, %r8
        jnz card_remove_user_card_remove

card_remove_user_card_set_bit:
        ## We need to tell the dealer he can replay that card now
        ## so we need to calculate where it is in the array
        movb (dealers_hand), %r10b
        incb %r10b
        movb %r10b, dealers_hand

        ## %rsi contains the suit of the card so if we
        subb $20, %sil

card_remove_user_card_set_bit_loop:
        test %sil, %sil
        jz card_remove_user_card_set_bit_end

        ## Thirteen is the number of cards in each suit
        addq $13, %rdx
        decb %sil
        jmp card_remove_user_card_set_bit_loop

card_remove_user_card_set_bit_end:
        ## We want to subtract two our array is zero-indexed but our cards start at 2.
        subq $2, %rdx
        ## We do that times two since each cards consists out of four bits
        shlq $2, %rdx
        ## We add one to offset the filler bit
        addq $1, %rdx
        ## We set the enabled bit to true.
        movb $0, dealers_hand(%rdx)

        movq %rbp, %rsp
        popq %rbp
        ret

###
### Card data string
###
card2data:
        pushq %rbp
        movq %rsp, %rbp

        movzbq (%rdi), %rdx
        incq %rdi
        movzbq (%rdi), %rcx
        incq %rdi
        movzbq (%rdi), %r8
        incq %rdi

        pushq %rdx
        pushq %rcx
        pushq %r8
        movq $128, %rdi
        call malloc
        movq %rax, %rdi
        popq %r8
        popq %rcx
        popq %rdx

        pushq %rdi
        movq $description, %rsi
        movq $0, %rax
        call sprintf
        popq %rax

        movq %rbp, %rsp
        popq %rbp
        ret

###
### Prints a human readable string with the information about a given card
###
### Arguments:
###    %rdi - the location of the card
###
card2str:
        ## Set the stack pointer again since printf modifies the stack
        pushq %rbp
        movq %rsp, %rbp

        ## Move the first item into a register
        movb (%rdi), %sil
        incq %rdi

        ## Set the colour
        movq $black, %rcx
        cmpb $30, %sil
        je card2str_suit
        movq $red, %rcx

card2str_suit:
        ## Move the next byte into %r8 and use byte extension just to be sure
        movzbq (%rdi), %r8
        incq %rdi

        ## Subtract the enumeration offset and multiply by 8 to get an offset
        subq $20, %r8
        ## Add the location of the first function.
        call *card2str_suit_table(,%r8,8)

card2str_suit_table:
        .quad .Lcard2str_spades
        .quad .Lcard2str_hearts
        .quad .Lcard2str_clubs
        .quad .Lcard2str_diamonds

.Lcard2str_spades:
        movq $spades, %rcx
        jmp card2str_get_value
.Lcard2str_hearts:
        movq $hearts, %rcx
        jmp card2str_get_value
.Lcard2str_clubs:
        movq $clubs, %rcx
        jmp card2str_get_value
.Lcard2str_diamonds:
        movq $diamonds, %rcx

card2str_get_value:
        ## Same steps as print suit
        movzbq (%rdi), %r8
        cmpq $10, %r8
        jl .Lcard2str_integer
        subq $10, %r8
        call *card2str_value_table(,%r8,8)

card2str_value_table:
        .quad .Lcard2str_ten
        .quad .Lcard2str_jack
        .quad .Lcard2str_queen
        .quad .Lcard2str_king
        .quad .Lcard2str_ace
.Lcard2str_integer:
        ## Create some space on the stack to store a new string
        subq $16, %rsp

        ## We add the ASCII value of '0' to get
        addq $'0, %r8

        ## We move the ASCII value number onto the stack together with
        ## a null character to create a new string
        movq %r8, -16(%rbp)
        movq $0, -8(%rbp)

        ## We give that location as an argument to pritnf
        leaq -16(%rbp), %rdx

        ## We print the character
        jmp card2str_string

.Lcard2str_ten:
        movq $ten, %rdx
        jmp card2str_string
.Lcard2str_jack:
        movq $jack, %rdx
        jmp card2str_string
.Lcard2str_queen:
        movq $queen, %rdx
        jmp card2str_string
.Lcard2str_king:
        movq $king, %rdx
        jmp card2str_string
.Lcard2str_ace:
        movq $ace, %rdx

card2str_string:
        ## FREE THIS FOR FUCKS SAKE YOU FUCK
        pushq %rdi
        pushq %rsi
        pushq %rdx
        pushq %rcx

        movq $128, %rdi
        call malloc
        movq %rax, %rdi
        popq %rcx
        popq %rdx
        popq %rsi

        ## Set the last bits to print it
        pushq %rdi
        movq $card_print_str, %rsi
        movq $0, %rax
        call sprintf
        popq %rax
        movq %r12, %r9

        ## Reset our stack pointer and return
        movq %rbp, %rsp
        popq %rbp
        ret

###
### Prints all the cards in the users hand
###
print_users_hand:
    ## Save the users_hand pointer in a register so we can use it as a pointer
    movq $users_hand, %rdi
    movzbq (%rdi), %rsi
    incq %rdi

    test %rdi, %rdi
    jz print_users_hand_end

print_users_hand_loop:
    test %rsi, %rsi
    jz print_users_hand_end

    push %rdi
    push %rsi

    call card2str

    pushq %rax
    movq %rax, %rdi
    movq $0, %rax
    call printf
    popq %rdi
    call free

    popq %rsi
    popq %rdi

    addq $4, %rdi
    decq %rsi

    jmp print_users_hand_loop

print_users_hand_end:
    ret

###
### Initializes the card array and fills it will all cards from every syuit
###
initialize_cards:
    movq $20, %rcx               # suit index
    movq $2, %rdx                # card value
    movq $30, %rbx               # the colour
    movq $dealers_hand, %rsi     # a pointer to the cards

    ## Set the length of the cards to 51
    movb $50, (%rsi)
    incq %rsi

initialize_cards_add_card:
    ## There are fourteen supported values
    cmpq $14, %rdx
    jg initialize_cards_next_suit

    ## Set the card to be enabled, which is to say, not in the user's hand already
    movb $0, (%rsi)
    incq %rsi

    ## Move the colour, suit and value into the array
    movb %bl, (%rsi)
    incq %rsi
    movb %cl, (%rsi)
    incq %rsi
    movb %dl, (%rsi)
    incq %rsi

    ## Move the card value up by one.
    incq %rdx
    jmp initialize_cards_add_card

initialize_cards_next_suit:
    ## If we are black we want to be red and vice versa
    movq $2, %rdx

    cmpq $30, %rbx
    movq $31, %rbx
    je 1f                       # Weird notation but this a local label, which is a thing.

    movq $30, %rbx
1:
    incq %rcx
    cmpq $23, %rcx
    jle initialize_cards_add_card
    ret

.bss
current_card:      .fill 3, 1        # Space for the colour, suit and value
previous_card:     .fill 3, 1        # Space for the colour, suit and value
users_hand:        .fill 1 + 54*4, 1 # 54 cards plus a length and an offset bit
dealers_hand:     .fill 1 + 54*4, 1  # 54 cards plus a length and an enabled bit per card
thrown_cards:      .fill 5100, 1     # Equivalent to around 1700 cards
