###
### Copyright (C) 2019, Valentijn van de Beek <valentijn@posteo.net>
###
### This file is part of NMao
###
### NMao is free software; You can redistribute it and/or modify
### it under the tersm of the GNU Public License as published by
### the Free Software Foundation, either version 3 of the License
### or (at your option) any later version.
###
### NMao is distributed with the hope that it will be useful, but
### WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU Public License for more details.
###
### By using NMao or any derived work you accept the superiority of
### the GNU Emacs text editor over ANY AND ALL other text editors
### including, but not limited to, vi improved for PERPETUITY. This
### requirement is IRREVOCABLE and applies to ALL USERS.
###
### You should have received a copy of the GNU Public License along
### with NMao. If not, see <https://gnu.org/licenses/>
###

.include "constants.s"
.include "functions.s"

.global compile_file, program, program_array, process_argument

###
### Process a piece of string into the equivalent argument or nested call
###
### Arguments:
###    %sil - the character indicating the argument
###
### Returns:
###    the argument or nested call in %rax
###
process_argument:
        push %rbp
        movq %rsp, %rbp

        ## The way the syntax is defined we can kinda cheat a bit
        ## because all the first letters in each possible type is
        ## unique so we only need to test those.
        movq $CURRENT_CARD, %rdx
        cmpb $'C, %sil
        je process_property

        movq $PREVIOUS_CARD, %rdx
        cmpb $'P, %sil
        je process_property

        movq $DEALERS_HAND, %rdx
        cmpb $'D, %sil
        je process_property

        movq $THROWN_CARDS, %rdx
        cmpb $'T, %sil
        je process_property

        movq $USERS_HAND, %rdx
        cmpb $'U, %sil
        je process_property

        ## The if the number is one then we know it must be 10
        ## this could be more elegant since this produces weird results
        ## for stuff like 1 + 1 = 20
        movq $10, %rax
        cmpb $'1, %sil
        je process_end

        movzbq %sil, %rax
        subq $'0, %rax
        cmpb $'9, %sil
        jle process_end

        movq $JACK, %rax
        cmpb $'j, %sil
        je process_end

        movq $QUEEN, %rax
        cmpb $'q, %sil
        je process_end

        movq $KING, %rax
        cmpb $'k, %sil
        je process_end

        movq $ACE, %rax
        cmpb $'a, %sil
        je process_end

        movq $SPADES, %rax
        cmpb $'s, %sil
        je process_end

        movq $HEARTS, %rax
        cmpb $'h, %sil
        je process_end

        movq $CLUBS, %rax
        cmpb $'c, %sil
        je process_end

        movq $DIAMONDS, %rax
        cmpb $'d, %sil
        je process_end

        movq $RED, %rax
        cmpb $'r, %sil
        je process_end

        movq $BLACK, %rax
        cmpb $'b, %sil
        je process_end

###
### If have some special value we also want some property
###
process_property:
        call find_breaking_character
        movb (%rdi), %sil

        movq $SV_COLOUR, %rcx
        cmpb $'c, %sil
        je process_create_property_call

        movq $SV_SUIT, %rcx
        cmpb $'s, %sil
        je process_create_property_call

        movq $SV_VALUE, %rcx
        cmpb $'v, %sil
        je process_create_property_call

        movq $SV_LENGTH, %rcx
        cmpb $'L, %sil
        je process_create_property_call

        movq $SV_DECK, %rcx
        cmpb $'d, %sil

###
### Encode it as an argument
###
process_create_property_call:
        pushq %rdi
        movq $get_property, %rdi
        movq %rdx, %rsi
        movq %rcx, %rdx
        call create_argument
        popq %rdi

process_end:
        movq %rbp, %rsp
        popq %rbp
        ret

###
### Adds an argument to the argument list.
###
### Arguments:
###  %rdi - the function you want to call
###  %rsi - the first argument
###  %rdx - the second argument
###
### Returns:
###   The address to the argument
###
create_argument:
        pushq %rbp
        movq %rsp, %rbp
        ## Save the values our arguments
        pushq %rdi
        pushq %rsi
        pushq %rdx

        ## Allocate space on the heap (we'll never free lol)
        movq $64, %rdi
        call malloc

        popq %rdx
        popq %rsi
        popq %rdi

        ## Move the values into our memory location
        movq %rax, %rcx
        movq %rdi, (%rcx)
        addq $8, %rcx
        movq %rsi, (%rcx)
        addq $8, %rcx
        movq %rdx, (%rcx)

        ## Restore the stack pointer and return
        movq %rbp, %rsp
        popq %rbp
        ret

###
### A helper function which takes a string and returns the first
### character past an ending character such as . or an empty space
###
### Note that this is a destructive function
###
### Arguments:
###  %rdi - a pointer to a string
###
find_breaking_character:
        movb (%rdi), %sil
        incq %rdi

        cmpb $'., %sil
        je find_breaking_character_end

        cmpb $32, %sil
        je find_breaking_character_end

        cmpb $40, %sil
        je find_breaking_character_end

        cmpb $41, %sil
        je find_breaking_character_end

        jmp find_breaking_character

find_breaking_character_end:
        ret

###
### Compile a given file into an array
###
compile_line:
        pushq %rbp
        movq %rsp, %rbp

        ## Move the first character into a register
        movb (%rdi), %sil
        incq %rdi

        ## If it is an opening bracket then it is a nested rule
        cmpb $40, %sil
        je compile_line_first_nested

        ## If it isn't we can find the argument type and store it
        call process_argument
        movq %rax, %rdx
        jmp compile_line_operation

compile_line_first_nested:
        ## Compile the nested rule
        call compile_line

        ## Create the nested rule as an argument
        pushq %rdi
        movq %r8, %rdi
        movq %rdx, %rsi
        movq %r9, %rdx
        call create_argument
        popq %rdi

        ## Set that as the first argument
        movq %rax, %rdx
        call find_breaking_character

compile_line_operation:
        ## Find the next . or space
        call find_breaking_character
        movb (%rdi), %sil

        movq $mao_plus, %r8
        cmpb $'+, %sil
        je compile_line_value

        movq $mao_minus, %r8
        cmpb $'-, %sil
        je compile_line_value

        movq $mao_times, %r8
        cmpb $'*, %sil
        je compile_line_value

        movq $mao_equal, %r8
        cmpb $'=, %sil
        je compile_line_value

        movq $mao_greater, %r8
        cmpb $'>, %sil
        je compile_line_value

        movq $mao_less, %r8
        cmpb $'<, %sil
        je compile_line_value

        movq $mao_or, %r8
        cmpb $'|, %sil
        je compile_line_value

        movq $mao_and, %r8
        cmpb $'&, %sil
        je compile_line_value

compile_line_value:
        ## This does basically the same thing as the first section
        call find_breaking_character
        movb (%rdi), %sil
        incq %rdi

        cmpb $40, %sil
        je compile_line_second_nested

        pushq %rdx
        call process_argument
        movq %rax, %r9
        popq %rdx

        jmp compile_line_message

compile_line_second_nested:
        pushq %rdx
        pushq %r8

        call compile_line

        pushq %rdi
        movq %r8, %rdi
        movq %rdx, %rsi
        movq %r9, %rdx

        call create_argument

        movq %rax, %r9
        popq %rdi
        popq %r8
        popq %rdx

        call find_breaking_character

compile_line_message:
        movq %rbp, %rsp
        popq %rbp
        ret

###
### Compiles a file into an array
###
### Arguments:
###   %rdi - a string containing a program
###
### Returns:
###   an array with the arguments in %rax
###
compile_file:
        pushq %rbp
        pushq %r12
        movq %rsp, %rbp
        movq $program_array, %r12

compile_file_run:
        ## Compile the first line into a rule
        call compile_line

compile_file_find_end_line:
        ## Find the end of a line
        ## TODO: replace with a repnz?
        movb (%rdi), %sil
        incq %rdi

        cmpb $32, %sil
        jne compile_file_find_end_line

        ## We want to move up by one to get to the message
        incq %rdi

        ## Allocate 1024 bytes of heap space
        ## again, never freed lmao
        pushq %rdi
        pushq %r8
        pushq %rdx
        pushq %r9
        movq $256, %rdi
        call malloc
        movq %rax, %rcx
        popq %r9
        popq %rdx
        popq %r8
        popq %rdi

compile_find_message:
        ## Go to the string until we found the ending ' or "
        ## TODO: also replace with repnz
        movb (%rdi), %sil
        incq %rdi

        cmpb $34, %sil
        je compile_find_message_end

        cmpb $39, %sil
        je compile_find_message_end

        ## Since this isn't the end, my friend, we copy the value into our buffer
        movb %sil, (%rcx)
        incq %rcx

        jmp compile_find_message

compile_find_message_end:
        ## We want to end the string with a null character
        movq $0, (%rcx)

        ## Move all the values into the array
        movq %r8, (%r12)
        addq $8, %r12
        movq %rdx, (%r12)
        addq $8, %r12
        movq %r9, (%r12)
        addq $8, %r12
        movq %rax, (%r12)
        addq $8, %r12

        ## Check if we reached the end of the file
        incq %rdi
        cmpb $0, (%rdi)
        jne compile_file_run

        ## At the end we can move our rule array into %rax and return it
        movq $program_array, %rax
        movq %rbp, %rsp
        popq %r12
        popq %rbp
        ret

###
###
###
.data
program_array:  .fill 512*4, 8
