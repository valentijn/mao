###
### Copyright (C) 2019, Valentijn van de Beek <valentijn@posteo.net>
###
### This file is part of NMao
###
### NMao is free software; You can redistribute it and/or modify
### it under the tersm of the GNU Public License as published by
### the Free Software Foundation, either version 3 of the License
### or (at your option) any later version.
###
### NMao is distributed with the hope that it will be useful, but
### WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU Public License for more details.
###
### By using NMao or any derived work you accept the superiority of
### the GNU Emacs text editor over ANY AND ALL other text editors
### including, but not limited to, vi improved for PERPETUITY. This
### requirement is IRREVOCABLE and applies to ALL USERS.
###
### You should have received a copy of the GNU Public License along
### with NMao. If not, see <https://gnu.org/licenses/>
###

###
### An enumeration which defines special values for cards, suits and colours.
### Each type is defined in a decimal section between 0 < n < 200
###
### Between 0  - 19 are card values which are defined as you would expect
### Between 20 - 29 are the suits in alternating order starting from spades
### Between 30 - 39 are the colours starting with black
###
.set JACK, 11
.set QUEEN, 12
.set KING, 13
.set ACE, 14

.set SPADES, 20
.set HEARTS, 21
.set CLUBS, 22
.set DIAMONDS, 23

.set BLACK, 30
.set RED, 31

###
### This defines an enumeration which defines the special type you want to
### read of. This can be any of Previous Card, Current Card, Thrown Cards,
### User Hand or Left Over Cards
###
.set CURRENT_CARD, 0
.set PREVIOUS_CARD, 1
.set USERS_HAND, 2
.set DEALERS_HAND, 3
.set THROWN_CARDS, 4

###
### An enumeration which defines the types of property that are available in the
### special values API.
###
.set SV_COLOUR, 0
.set SV_SUIT, 1
.set SV_VALUE, 2
.set SV_LENGTH, 3
.set SV_DECK, 4
