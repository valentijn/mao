#+TITLE: Design Document: "That is my Fanta!"
#+AUTHOR: Valentijn van de Beek (vvandebeek)
#+EMAIL: v.d.vandebeek@student.tudelft.nl

* Introduction
/"That my fanta!"/[1], as it is collequally known by TU freshman, or it's
proper name /Mao/ is a simple card game where there are a set of
unknown rules that the player has to decipher while playing the
game. The dealer will set the rules at the beginning of the game and
the players need to throw up a card every round. If they break one of
the rules or apply one superfluously they get a penalty card. I'll aim
to implement a text based version of this game.

In most versions of the game, the winning player can let the dealer
reveal to them a rule or add one to it but this will not be
implemented due to the fact that it will be single player.

To prevent this becoming a single shot game there will be a small
engine which will compile a given game file to a set of rules that
will be applied to the given game. This file is passed through the
commandline at beginning of the program and can't be set during the
execution of the program. For more information and an example file see
Appendix A.

Scores are kept in terms of the person who managed to beat the dealer
in the least amount of turns for a particular game file.

[1] The game was introduced to a set of freshmen during the
/Christiaan Huygen's/ freshman weekend by Theo ... where the most
memorable rule was one called "That is my fanta".

* Description
Here follows a short description of the steps that a program will take
and maybe UML if I am not lazy/got the time for it.

** Initialization
The program is begun by starting the executable with the game file as
the first argument and only argument. This is similar to the way that
the example Brainfuck interpeter

This will call the main function of the program which will read the
file, parse the file into a set of rules and will store that into a
register. It will then pick five random cards to deal to the user,
print a starting message and begin the game.

** Main loop
At the beginning of a round it will print the number of the round, the
cards that the user has in his hands and a prompt. Into this prompt
the user can write the suit, card that they want to play this turn and
press enter. Then they are faced by another set of prompts where they
can write down any applicable rule and for each of them they have to
press enter. At the end of the turn a user types end or . which will
end turn.

Then the dealer will look at the card, see what rules are applicable
and see if those parameters are given. For each that is broken or
superfluous it will assign a random card. If they are no more cards to
be given out, the player has lost the game.

If the player played his or her last card and is not given any penalty
cards then they have won.

** End Game
At the end of the game it will be announced until what round they
played, the highest cards they were penalized with and it will print
this to the screen. Then it will prompt the user for an username and,
if given, print the score onto a file called mao-scores.txt

* Appendix A
This appendix defines the game file syntax and will the describe the
way that will work in the assembly implemenation. Since this going to
be pretty meaty it will quite a few details and so I put it in a
seperate section.

First we'll jump into the cool, how I am going to do a bit which is
followed by a detailed explanation of the entire syntax, some rules
which are defined by some default file and and example of how a file
would look like. This latter part can be used as a reference to
understand how the rest of it works.

** The implementation
The fundemental problem, and as such the challenge, of this game is
going to be how to implement the parser for the file since it requires
some measure of metaprogramming which is Assembly is somewhat famous
for lacking. In fact, Assembly is to metaprogramming as I am to being
popular with girls or in fact any human contact whatsoever.

In order to get around this we will treat the game file in a rather
similar way that we treated the brainfuck program in the RLE optimized
version of the interpeter. Which is to compile the file into some
array in the form of some ID and a set of arguments. However this will
lead into the natural problem that we don't really know when the
arguments end and when the new function starts. However since we only
defined binary operations which only ever take at most and at least 2
variables we can just make the assumption that they are two
argument. This gives us a memory layout which look something like the
following: *[ID][arg1][arg2][msg]"* where the ID some way to indicate
what function to run, the two arguments given and message is that
should be taken if the the given card fits the rules.

As an ID we can use the labels of a predefined set of subroutines
which will execute the defined function. This will allow us to
simplify the handler by using an indirect loop to jump to the handler
function. The way we define arguments coincendentally sets us up to
also allows us to gracefully deal with the nested functions calls by
just defining one of the arguments as a pointer to a bit of the stack
which contains an array with how to deal with that function. This
function will execute first and return it's value. Due to the way that
the game logic works they can only do mathematical or logical
operations. In essence, it will function as some kind of glorified
linked list combined with a jump table.

Most of the properties can be dealt with by treating them an
enumeration whose type is indicated by the preceding values. To
simplify our code we'll treat them as one enumeration with each type
in it's own decimal, similar to HTTP exit codes. So 0-14 are the
cards, 20-23 are set aside for suits and 30 - 31 to red or black.

The special symbols *cc*, *pc*, *tc*, *uh*, and *hc* are given
constant places in memory and hence can just be translated to a memory
address which contains those values as an array or single
values. Access to it's properties are done by translating those to
nested calls.

** The execution
So far so good, we defined our game file as a runnable bit of code but
we're left with the fact that all we have now is basically just an
array of magic numbers with no real way to decode it. This is were our
lengthy description comes in useful since it describes exactly how we
can deduce exactly what it is by the value of the number itself.

First off, we can rule out that it is any label since those can only
occur at the first element of a function call. If the number is less
than 100 then we know that it some constant value since we saved our
nested calls on some place in the stack which grows down and so our
value most be some nested call. We can execute that call plus any
nested calls within it, return the value and finish our starting call.

To put it in psuedo-code, all it has to do is the following:

#+BEGIN_SRC python :results output
  import operator

  current_card = [30, 20, 4]   # 4 of hearts
  previous_card = [31, 21, 9]  # 9 of clubs
  leftover_cards = []          # You get the point

  thrown_cards = [(31, 21, 9), (30, 20, 4)]
  user_hands = [(30, 20, 5), (30, 20, 8),
                (31, 21, 14), (30, 23, 11)]


  # We need to put this here since we call use it as variable
  def get_current_card(value_, _):
      return current_card[value_]

  # We pretend that this dictionary is the stack
  operations = {210: (operator.add, 3, 2),
                214: (operator.sub, 9, 2),
                218: (get_current_card, 2, 0),
                222: (operator.sub, 218, 3)}

  # A flat array containing just functions, arguments and messages
  call = [operator.gt, 5, 4, "Goodbye cruel world",
          operator.lt, 5, 4, "I am leaving you today.",
          operator.eq, 5, 210, "Goodbye, goodbye, goodbye",
          operator.eq, 9, 214, "Goodbye all you people",
          operator.eq, 1, 222, "There is nothing you can say to "\
                  "change my mind.",
          operator.eq, 222, 1, "Goodbye."]

  def process_call(fun, arg1, arg2):
      # If either argument is out of our safe zone we want to
      # recursively call ourselves
      if arg1 > 40:
          # We use some neat Pythononic stuff here to make stuff easy.
          (nfun, narg1, narg2) = operations[arg1]
          arg1 = process_call(nfun, narg1, narg2)

      if arg2 > 40:
          (nfun, narg1, narg2) = operations[arg2]
          arg2 = process_call(nfun, narg1, narg2)

      # Return the value of our call
      return fun(arg1, arg2)


  # Call all the functions in our array
  for i in range(0, len(call), 4):
      if process_call(call[i], call[i + 1], call[i + 2]):
          print(call[i + 3])
#+END_SRC

#+RESULTS:
: Goodbye cruel world
: Goodbye, goodbye, goodbye
: There is nothing you can say to change my mind.
: Goodbye.

** Syntax
The syntax of the game file consists out functions and classes
with properties which you can use to create actions. The classes are
as follows:

- *cc*
Stands for Current Card and contains the card that the user just threw
up. It has as properties: *colour*, *suit* and *value*.

- *pc*
Stands for Previous Card and contains the card that the user
previously threw up. It has the same properties of *cc*

- *hearts*, *clubs*, *clovers*, and *diamonds*
Has it's colour and every card in their suit as a property.

- *red*, and *black*
Contains as properties the suits in their colour

- *uh*
Contains the user's hands and has the properties *cards* and
*length*. The former is an array with the cards that they have and the
latter is the cards they have left over

It also contains the following functions:
- *[card]*
Do something if this card is thrown

- *[card].[a] -> [card].[b]*
If the property switches to this property the user should do something

- *||*, and *&&*
Adds alternative or additional restrictions.

- *+*, *-*, ***, */*
Performs addition, substraction, multiplication and division
respectively. You can use it as such:\\
*tc.value -> tc + 3 || tc.value -> tc - 3*'

- *==*, *<*, and *>*
See if a value is equal, less or more.

- *:*
The user has to do the following.

- *"[string]"*
The user has to say the following thing

- *![action]!*
The user has to do the following thing

Furthermore there are these arrays.

- *tc*
An array with all the thrown cards

- *lc*
An array with the cards left over. Has the property length which
returns the cards left over

** Example of a game file
(pc.value == ace) && (tc.suit == clubs): "motorhead"\\
(pc.color == black) -> (cc.color == red): "That is my Fanta!"\\
(cc.value > (pc.value - 3)) || (cc.value < (pc.value + 3)): "Wow!"\\
(pc.value == 4) && (cc.value == 2): "Rule 1: never forget your towel"\\

** List of game rules
Here follows a list of game rules that are defined in the standard
game rule sets, be warned, it will spoil the game.

- *That is my Fanta!*
If you throw a card which is a different colour from the one that is
on top of the thrown cards you need to say "That is my Fanta!"

- *Motorhead*
If you throw an ace of spades you need to say Motorhead

- *Wow!*
If the card you throw is three more or less than the card below it you
need to say wow!

- *42*
If you throw any two on top of any four you need to say "Rule 1: never forget
your towel"
