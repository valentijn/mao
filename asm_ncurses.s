###
### This file defines some helper functions for ncurses
###
###
### Copyright (C) 2019, Valentijn van de Beek <valentijn@posteo.net>
###
### This file is part of NMao
###
### NMao is free software; You can redistribute it and/or modify
### it under the tersm of the GNU Public License as published by
### the Free Software Foundation, either version 3 of the License
### or (at your option) any later version.
###
### NMao is distributed with the hope that it will be useful, but
### WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU Public License for more details.
###
### By using NMao or any derived work you accept the superiority of
### the GNU Emacs text editor over ANY AND ALL other text editors
### including, but not limited to, vi improved for PERPETUITY. This
### requirement is IRREVOCABLE and applies to ALL USERS.
###
### You should have received a copy of the GNU Public License along
### with NMao. If not, see <https://gnu.org/licenses/>
###

.text
juststr: .asciz "%s"
mark:    .asciz " * "

.global ncurses_print_middle, ncurses_create_card_menu, ncurses_free_menu
###
### Prints a given string in the middle of the screen
###
### Arguments:
###    %rdi - the string you want to print
###    %rsi - row offset
###    %rdx - column offset
###
ncurses_print_middle:
        pushq %rbp
        movq %rsp, %rbp

        ## Save our arguments on the stack
        movq %rdi, %rcx
        ## Move the maximum lines into the rdx, subtract the offset
        ## and divide it by two. This will give us the exact middle
        ## of the screen
        movq (LINES), %rdi
        subq %rsi, %rdi
        shrq $1, %rdi
        subq $1, %rdi

        movq (COLS), %rsi
        subq %rdx, %rsi
        shrq $1, %rsi

        ## We just want to print the string and that is it.
        movq $juststr, %rdx

        call mvprintw

        movq %rbp, %rsp
        popq %rbp
        ret

###
### Create a menu with the cards and an exit option
###
ncurses_create_card_menu:
        ## Create a new stack frame and push the preserved registers
        pushq %rbp
        pushq %r12
        pushq %r13
        movq %rsp, %rbp

        ## Move the length of the user's into a preserved register
        movq $users_hand, %rdi
        movzbq (%rdi), %r12
        incq %rdi

        ## If there is nothing in his hand we want to just stop
        ## test %rdi, %rdi
        ## jz ncurses_create_card_menu_end

        ## Move the card menu variable into a register
        movq $cardmenu, %r13

ncurses_create_card_menu_loop:
        ## If we're done with the users hand we want to return
        test %r12, %r12
        jz ncurses_create_card_menu_end

        ## We save the user's hand
        pushq %rdi

        pushq %rdi
        pushq %rsi
        pushq %rdx
        pushq %rcx
        call card2data
        popq %rcx
        popq %rdx
        popq %rsi
        popq %rdi

        pushq %rax

        ## Convert it into a string
        call card2str

        ## Create an item with the card's name
        popq %r9
        movq %rax, %rdi
        movq %r9, %rsi
        call new_item

        ## Move it into the menu and go to the next value
        movq %rax, (%r13)
        addq $8, %r13

        ## Go to the next card
        popq %rdi
        addq $4, %rdi
        decq %r12

        jmp ncurses_create_card_menu_loop

ncurses_create_card_menu_end:
        ## Create the card and print it.
        movq $cardmenu, %rdi
        call new_menu
        movq %rax, %r12

        ## Disable showing the description
        movq %r12, %rdi
        movq $0x02, %rsi
        call menu_opts_off

        ## Set the menu format so we have columns
        movq %r12, %rdi
        movq $10, %rsi
        movq $5, %rdx
        call set_menu_format

        ## Set the mark
        movq %r12, %rdi
        movq $mark, %rsi
        call set_menu_mark

        movq %r12, %rax

        movq %rbp, %rsp
        popq %r13
        popq %r12
        popq %rbp
        ret

###
### Free the data inside the menu
###
ncurses_free_menu:
        pushq %rbp
        movq %rsp, %rbp

        ## Save the menu on the stack
        pushq %rdi

        ## First clean the items
        movq $cardmenu, %rdi

ncurses_free_menu_loop:
        ## Save the current argumnet
        pushq %rdi

        ## Free the item
        movq (%rdi), %rdi
        pushq %rdi
        call item_name
        movq %rax, %rdi
        call free
        popq %rdi

        pushq %rdi
        call item_description
        movq %rax, %rdi
        call free
        popq %rdi

        call free_item
        popq %rdi

        ## Zero out the current item
        movq $0, (%rdi)

        ## Go to the next items until we're all done
        addq $8, %rdi
        cmpq $0, (%rdi)
        jne ncurses_free_menu_loop

        ## Free the menu
        popq %rdi
        pushq %rdi
        call unpost_menu
        popq %rdi
        call free_menu

        movq %rbp, %rsp
        popq %rbp


.data
cardmenu:       .fill 128, 8
