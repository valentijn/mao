###
### This file defines the subroutines and arrays involved with evaluating a program
### array.
###
###

###
### Copyright (C) 2019, Valentijn van de Beek <valentijn@posteo.net>
###
### This file is part of NMao
###
### NMao is free software; You can redistribute it and/or modify
### it under the tersm of the GNU Public License as published by
### the Free Software Foundation, either version 3 of the License
### or (at your option) any later version.
###
### NMao is distributed with the hope that it will be useful, but
### WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU Public License for more details.
###
### By using NMao or any derived work you accept the superiority of
### the GNU Emacs text editor over ANY AND ALL other text editors
### including, but not limited to, vi improved for PERPETUITY. This
### requirement is IRREVOCABLE and applies to ALL USERS.
###
### You should have received a copy of the GNU Public License along
### with NMao. If not, see <https://gnu.org/licenses/>
###

.include "functions.s"
.include "constants.s"

.global evaluate_card, process_call

.text
juststr:        .asciz "%s\n"


###
### Adds an argument to the argument list.
###
### Arguments:
###  %rdi - the function you want to call
###  %rsi - the first argument
###  %rdx - the second argument
###
### Returns:
###   The address to the argument
###
create_argument:
        pushq %rbp
        movq %rsp, %rbp
        ## Save the values our arguments
        pushq %rdi
        pushq %rsi
        pushq %rdx

        ## Allocate space on the heap (we'll never free lol)
        movq $24, %rdi
        call malloc

        popq %rdx
        popq %rsi
        popq %rdi

        ## Move the values into our memory location
        movq %rax, %rcx
        movq %rdi, (%rcx)
        addq $8, %rcx
        movq %rsi, (%rcx)
        addq $8, %rcx
        movq %rdx, (%rcx)

        ## Restore the stack pointer and return
        movq %rbp, %rsp
        popq %rbp
        ret

###
### Call nested function
###
### Calls the appropiate nested function
###
### Arguments:
###   %rdi - a tuple containing the function and the arguments you want to return
###
### Returns:
###   The returned value of the function ran in %rax
###
run_nested_function:
        ## Move the first argument into a temporary register since
        ## we'll overwrite it instantly
        movq %rdi, %rcx

        ## Load the values of the list as arguments and recursively
        ## call ourselves
        movq (%rcx), %rdi
        addq $8, %rcx
        movq (%rcx), %rsi
        addq $8, %rcx
        movq (%rcx), %rdx
        call process_call
        ret

###
### Evaluates a rule and any nested rules within it.
###
### Arguments:
###   %rdi - a function label
###   %rsi - the first argument
###   %rdx - the second argument
###
### Returns:
###    the end result of the rule and any rules nested in it.
###
process_call:
        ## Set the stack pointer
        pushq %rbp
        movq %rsp, %rbp

        ## If the value is below 200 it must be some special value
        ## those are defined by constants.s
        cmpq $200, %rsi
        jle process_card_second_argument

        ## Save the values of %rdi and %rsi so we can use them later
        pushq %rdi
        pushq %rdx
        movq %rsi, %rdi
        call run_nested_function
        ## Restore the values
        popq %rdx
        popq %rdi

        ## Set the return value as the first argument
        movq %rax, %rsi

process_card_second_argument:
        ## If the second argument is more than 200 then it some special value
        cmpq $200, %rdx
        jle process_card_run

        ## Save the values of the function and the first argument
        pushq %rdi
        pushq %rsi
        movq %rdx, %rdi
        call run_nested_function

        ## Restore the values of the first argument and the function
        popq %rsi
        popq %rdi

        ## Set the return value as the second argument
        movq %rax, %rdx

process_card_run:
        ## Move the function you want to run to %rcx and the argument to
        ## the C convention mandated registers
        movq %rdi, %rcx
        movq %rsi, %rdi
        movq %rdx, %rsi

        call *%rcx

        movq %rbp, %rsp
        popq %rbp
        ret

###
### Evaluates the program array and checks the current card for every rule in that array
###
###
### Returns:
###   It returns 0 if it passes all rules or N for the amount of rules it fails.
###
evaluate_card:
        pushq %rbp
        pushq %r12
        pushq %r13
        movq %rsp, %rbp
        movq %rdi, %r12

        ## Create an array
        movq $512, %rdi         # Create an array for 64 strings
        call malloc
        movq %rax, %r13
        pushq %r13

evaluate_card_run:
        ## Move the values from the array into the registers
        movq (%r12), %rdi
        addq $8, %r12
        movq (%r12), %rsi
        addq $8, %r12
        movq (%r12), %rdx
        addq $8, %r12

        ## Process the card
        call process_call

        ## If the card is true we want to check whether a user entered
        ## the right secret phrase
        cmpq $1, %rax
        jne evaluate_card_check

        ## For now we just print it
        movq (%r12), %rdi
        movq %rdi, (%r13)
        addq $8, %r13

evaluate_card_check:
        ## We move to the next rule in the array
        addq $8, %r12
        cmpb $0, (%r12)
        jne evaluate_card_run

        popq %rax

        movq %rbp, %rsp
        popq %r13
        popq %r12
        popq %rbp
        ret
