# Nao
=====

This is a version of the [Mao card game](https://en.wikipedia.org/wiki/Mao_(card_game))
written in x86_64 Assembly using the GNU Assembler with AT&T syntax.
It depends on the GNU Libc, but should work on any, and also the
NCurses library to draw the terminal interface.

The game works by picking a card and typing every rule applicable to
that card. You're not told what the rules are and you need to figure
it out as you play. There is a dealer which will display the rules
you broke and will deal you a card for each one.

## Keys
RET - Play the given card
q - exit the game

## Copyright
Everything is licensed under GNU Public License v3 except read_file.s and
lines until I call compile file since those are taking from code given to
me by the teachers.
