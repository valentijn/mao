CC=gcc
CFLAGS=-D_DEFAULT_SOURCE -D_XOPEN_SOURCE=600
LIBS=-lmenu -lncurses -ltinfo
objects = build/cards.o build/mao.o build/evaluate.o build/compile.o build/read_file.o build/asm_ncurses.o

.PHONY: clean

mao: $(objects)
	$(CC) -g -no-pie ${LIBS} ${CFLAGS} -o "$@" $^ 

build:
	mkdir build

build/%.o: %.s | build
	$(CC) -g -no-pie -c -o "$@" "$<" -g

clean:
	rm -Rf mao build
