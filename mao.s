###
### Copyright (C) 2019, Valentijn van de Beek <valentijn@posteo.net>
###
### This file is part of NMao
###
### NMao is free software; You can redistribute it and/or modify
### it under the tersm of the GNU Public License as published by
### the Free Software Foundation, either version 3 of the License
### or (at your option) any later version.
###
### NMao is distributed with the hope that it will be useful, but
### WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU Public License for more details.
###
### By using NMao or any derived work you accept the superiority of
### the GNU Emacs text editor over ANY AND ALL other text editors
### including, but not limited to, vi improved for PERPETUITY. This
### requirement is IRREVOCABLE and applies to ALL USERS.
###
### You should have received a copy of the GNU Public License along
### with NMao. If not, see <https://gnu.org/licenses/>
###
.altmacro

.include "constants.s"
.global main

.text
title:  .asciz "The Mao Card Game"
author:  .asciz "written by Valentijn van de Beek"
idea:   .asciz "Based on the idea of Teo."
heaven: .asciz "He is in Romanian heaven now."
youwin: .asciz "You have been arrested for going against the party."
congrats: .asciz "https://www.youtube.com/watch?v=1Bix44C1EzY"
subtitle: .asciz "Can you guess the rules?"
prompt: .asciz "> "
usage_format: .asciz "usage: %s <filename>\n"
juststr: .asciz "%s"

main:
        pushq %rbp
        movq %rsp, %rbp
        subq $16, %rsp

        # Make sure we got one argument.
        # The first argument is always the name of our program, so we want a second argument.
        cmp $2, %rdi
        jne wrong_argc

        # Read the file with the brainfuck code.
        # 8(%rsi) is argv[1], the path of the file we should read.
        # See read_file.s for details on the read_file subroutine.
        movq 8(%rsi), %rdi
        leaq -8(%rbp), %rsi
        call read_file
        test %rax, %rax
        jz failed
        movq %rax, -16(%rbp)
        movq %rax, %rdi

        call compile_file

        # Free the buffer allocated by read_file.
        movq -16(%rbp), %rdi
        call free
        addq $16, %rsp

        call initscr            # start ncurses
        call cbreak             # disable buffering
        call noecho             # Disable echoing
        call nonl               # Don't detect newlines

        ## Invisible cursor
        movq $0, %rdi
        call curs_set

        movq (stdscr), %rdi
        movq $0, %rsi
        call intrflush          # Disable flushing on interrupt

        ## Enable Function keys
        movq (stdscr), %rdi
        movq $1, %rsi
        call keypad

        movq $title, %rdi
        movq $0, %rsi
        movq $17, %rdx
        call ncurses_print_middle

        movq $author, %rdi
        movq $-2, %rsi
        movq $32, %rdx
        call ncurses_print_middle

        movq $idea, %rdi
        movq $-4, %rsi
        movq $30, %rdx
        call ncurses_print_middle

        movq $subtitle, %rdi
        movq $-10, %rsi
        movq $24, %rdx
        call ncurses_print_middle

        ## Display our title screen
        call refresh

        ## Wait for input
        call getch

        ## Clear the screen
        call clear

        ## We want to set the length into the thrown cards in a preserved register
        movq $0, %r12

        ############################
        ## Initialize the program ##
        ############################
        call initialize_cards

        ## Get the first five cards that the user can play
        .rep 5
        call card_get
        .endr


        ####################################
        ## Create the played cards window ##
        ####################################
        movq (LINES), %rdi
        shr $1, %rdi
        movq %rdi, (maxlinecount)
        decq (maxlinecount)
        movq $30, %rsi
        movq $2, %rdx
        movq $5, %rcx
        call newwin
        movq %rax, (played_cards)

        ## Give it a box
        movq (played_cards), %rdi
        movq $0, %rsi
        movq $0, %rdx
        call box

        ## Refresh the window
        call refresh
        movq (played_cards), %rdi
        call wrefresh

        #####################################
        ## Create a box for dealing output ##
        #####################################
        movq (LINES), %rdi
        shrq $2, %rdi
        movq (COLS), %rsi
        subq $45, %rsi
        movq $2, %rdx
        movq $80, %rcx
        shrq $1, %rcx
        call newwin
        movq %rax, (dealer_win)

        ## Give it a box
        movq (dealer_win), %rdi
        movq $0, %rsi
        movq $0, %rdx
        call box

        ## Refresh the window
        call refresh
        movq (dealer_win), %rdi
        call wrefresh

        ########################################
        ## Create a box for inputting strings ##
        ########################################
        movq (LINES), %rdi      # number of  lines
        shrq $2, %rdi           # divide by two
        movq (COLS), %rsi
        subq $45, %rsi          # number of columns
        movq (LINES), %rdx      # Move the number of lines of the offset
        subq $9, %rdx           # Subtract a magical number
        shrq $1, %rdx           # Divide by two
        movq $80, %rcx          # Add a magical number
        shrq $1, %rcx           # Divide by two
        call newwin             # Create a window
        movq %rax, (input_win)  # Move it into a dealer_win

        ## Give it a box
        movq (input_win), %rdi  # Give the window as an argument
        movq $0, %rsi           # Could be cooler
        movq $0, %rdx
        call box

        ## Refresh the window
        call refresh
        movq (input_win), %rdi
        call wrefresh


main_begin_round:
        call ncurses_create_card_menu
        movq %rax, %r13

        ## Create a new window to put the menu into
        movq $10, %rdi
        movq (COLS), %rsi
        subq $10, %rsi
        movq (LINES), %rdx
        subq $11, %rdx
        movq $5, %rcx
        call newwin
        movq %rax, (menu_win)

        ## Set the menu to a subwindow
        movq %r13, %rdi
        movq (menu_win), %rsi
        call set_menu_win

        ## Create a subwindow
        movq (menu_win), %rdi
        movq $6, %rsi
        movq (COLS), %rdx
        subq $15, %rdx
        movq $3, %rcx
        movq $1, %r8
        call derwin
        movq %rax, (menu_sub)

        ## Set the item window
        movq %r13, %rdi
        movq (menu_sub), %rsi
        call set_menu_sub

        ## Create a boc around the window
        movq (menu_win), %rdi
        movq $0, %rsi
        movq $0, %rdx
        call box

        ## Enable special characters for this window
        movq (menu_win), %rdi
        movq $1, %rsi
        call keypad

        ## Print the menu
        movq %r13, %rdi
        call post_menu
        call refresh

main_round_loop:
        movq (played_cards), %rdi
        call wrefresh

        movq (menu_win), %rdi
        call wrefresh

        movq (dealer_win), %rdi
        call wrefresh

        movb (users_hand), %dil
        cmpb $0, %dil
        je you_win

        ## Get a character
        call getch
        movq %rax, %rsi

        cmpq $'q, %rsi
        je main_exit

        ## Check if KEY_DOWN was pressed
        cmpq $0402, %rsi
        je menu_down_item

        ## Check if KEY_UP was pressed
        cmpq $0403, %rsi
        je menu_up_item

        cmpq $0404, %rsi
        je menu_left_item

        cmpq $0405, %rsi
        je menu_right_item

        cmpq $13, %rsi
        je play_card

        cmpq $'h, %rsi
        je main_round_loop_exit

main_round_loop_end:
        jmp main_round_loop

menu_up_item:
        ## Call the menu driver to go up one item
        movq %r13, %rdi
        movq $0777, %rsi
        addq $3, %rsi
        call menu_driver
        jmp main_round_loop_end

menu_down_item:
        ## Call the menu driver to go down one item
        movq %r13, %rdi
        movq $0777, %rsi
        addq $4, %rsi
        call menu_driver
        jmp main_round_loop_end

menu_left_item:
        ## Call the menu driver to go down one item
        movq %r13, %rdi
        movq $0777, %rsi
        addq $1, %rsi
        call menu_driver
        jmp main_round_loop_end

menu_right_item:
        ## Call the menu driver to go down one item
        movq %r13, %rdi
        movq $0777, %rsi
        addq $2, %rsi
        call menu_driver
        jmp main_round_loop_end

###
### Play the currently selected item as the card
###
play_card:
        pushq %rbp
        movq %rsp, %rbp

        movq (maxlinecount), %rdi
        cmpq %rdi, (linecount)
        jne play_card_continue

        movq $1, (linecount)
        movq (played_cards), %rdi
        call wclear

        movq (played_cards), %rdi
        movq $0, %rsi
        movq $0, %rdx
        call box

play_card_continue:
        ## First we want to clear the dealer's window
        movq (dealer_win), %rdi
        call wclear

        ## Then we clear the input window
        movq (input_win), %rdi
        call wclear

        ## Redraw the box
        movq (dealer_win), %rdi
        movq $0, %rsi
        movq $0, %rdx
        call box

        ## Redraw the box
        movq (input_win), %rdi
        movq $0, %rsi
        movq $0, %rdx
        call box

        ## Get the name of the current item
        movq %r13, %rdi
        call current_item
        movq %rax, %rdi
        call item_description
        movq %rax, %rdi

        ## Turn the description into the correct values to create the card
        call get_integer
        pushq %rax
        call get_integer
        pushq %rax
        call get_integer
        movq %rax, %rdx

        ## Play the card
        popq %rsi
        popq %rdi
        call card_play

        ## Get the card from memory
        movq %r13, %rdi
        call current_item
        movq %rax, %rdi
        call item_name

        ## Print the card onto the screen
        movq (played_cards), %rdi # Window
        movq (linecount), %rsi    # The line offset
        movq $2, %rdx             # Column offset
        movq $juststr, %rcx       # The format string
        movq %rax, %r8            # The card name
        movq $0, %rax             # We need to set this to zero before printf freaks out
        call mvwprintw            # Print the line on the screen
        incq (linecount)          # We can now increment by one

        movq $program_array, %rdi
        call evaluate_card
        movq %rax, %rdx
        pushq %rdx
        call get_user_input
        popq %rdx
        pushq %rdx

        ## Print the rules onto the screen
        movq $1, %r9
play_card_loop:
        cmpq $0, (%rdx)
        je play_card_end
        movq $user_input_buffer, %r10

play_card_check_equal:
        movq (%rdx), %rdi
        movq (%r10), %rsi

        cmp $0, %rsi
        je play_card_check_print

        ## This is definitely, definitely cheating
        pushq %rdx
        pushq %r9
        pushq %r10
        call strcmp
        popq %r10
        popq %r9
        popq %rdx

        addq $8, %r10

        cmpq $0, %rax
        jne play_card_check_equal

        addq $8, %rdx
        jmp play_card_loop

play_card_check_print:
        pushq %rdx
        pushq %r9
        pushq %r10

        movq (dealer_win), %rdi # The window we want to print on
        movq (%rdx), %r8        # The thing we want to print
        movq %r9, %rsi          # The line offset
        movq $2, %rdx           # Column offset
        movq $juststr, %rcx     # The format string
        movq $0, %rax           # Else printf will freak out
        call mvwprintw

        call card_get
        cmpq $-1, %rax
        je you_win

        popq %r10
        popq %r9
        popq %rdx

        incq %r9

        ## We move by eight since we are storing the pointers
        ## in quads
        addq $8, %rdx
        jmp play_card_loop

play_card_end:
        ## Free our memory
        popq %rdi
        call free

        movq $user_input_buffer, %rdi

play_card_end_clean:
        pushq %rdi
        cmpq $0, (%rdi)
        je play_card_end_clean_end

        movq (%rdi), %rdi
        call free
        popq %rdi
        ## Zero out our memory
        movq $0, (%rdi)
        addq $8, %rdi

        jmp play_card_end_clean

play_card_end_clean_end:
        movq %rbp, %rsp
        popq %rbp
        jmp main_round_loop_exit

###
### A destructive operation which gets an integer from a string
###
get_integer:
        pushq %rbp
        movq %rsp, %rbp

        ## Create some place on the stack to the store the integer
        subq $4, %rsp

        ## Build a string of the given itneger
        movb (%rdi), %sil
        movb %sil, -3(%rbp)
        incq %rdi
        movb (%rdi), %sil
        movb %sil, -2(%rbp)
        incq %rdi
        movb $0, -1(%rbp)
        incq %rdi

        ## Save the current place of the string and translate the integer
        ## to a number
        pushq %rdi
        leaq -3(%rbp), %rdi
        call atoi
        popq %rdi

        movq %rbp, %rsp
        popq %rbp
        ret

###
### Get user input
###
get_user_input:
        pushq %rbp
        pushq %r12
        pushq %r13
        movq %rsp, %rbp

        ## Reset the line counter
        movq $1, (inputcount)

        movq $user_input_buffer, %r13
        ## Enable echoing of character
        call echo

        ## Show cursor
        movq $1, %rdi
        call curs_set

get_user_input_loop:
        ## Free this you absolute tosser
        movq $512, %rdi
        call malloc
        movq %rax, %r12

        ## Print a prompt
        movq (input_win), %rdi  # Give the window as an argumnet
        movq (inputcount), %rsi # Line offset
        movq $2, %rdx           # Column offset
        movq $prompt, %rcx      # Prompt
        call mvwaddstr          # Print the string

        movq (input_win), %rdi  # Give the window as an argument
        movq (inputcount), %rsi # Line offset
        movq $4, %rdx           # Column offset
        movq %r12, %rcx         # Store the output
        call mvwgetstr          # get the string

        cmpq $0, (%r12)
        je get_user_input_end

        movq %r12, (%r13)
        addq $8, %r13

        ## Increment the lines of input by one
        incq (inputcount)

        ## Refresh the window
        movq (input_win), %rdi
        call wrefresh
        jmp get_user_input_loop

get_user_input_end:
        ## Disable echo
        call noecho

        ## Hide cursor
        movq $0, %rdi
        call curs_set
        movq $user_input_buffer, %r13

get_user_input_clean:
        movq %r13, %rax
        movq %rbp, %rsp
        popq %r13
        popq %r12
        popq %rbp
        ret

main_round_loop_exit:
        movq %r13, %rdi
        call ncurses_free_menu

        ## Exit the current loop but first cleanup the menu so we don't
        ## leak like an old man.
        movq (menu_sub), %rdi
        call delwin

        movq (menu_win), %rdi
        call delwin


        jmp main_begin_round

you_win:
        call clear

        movq $youwin, %rdi
        movq $0, %rsi
        movq $50, %rdx
        call ncurses_print_middle

        movq $congrats, %rdi
        movq $-4, %rsi
        movq $45, %rdx
        call ncurses_print_middle

        call refresh
        call getch

main_exit:
        ## Cleanup all the windows
        movq (menu_sub), %rdi
        call delwin

        movq (menu_win), %rdi
        call delwin

        movq (played_cards), %rdi
        call delwin

        movq (dealer_win), %rdi
        call delwin

        movq (input_win), %rdi
        call delwin

        call endwin
        movq $stdscr, %rdi
        call delscreen

        movq %rbp, %rsp
        popq %rbp
        movq $0, %rax
        ret

get_color_table:
        .byte 30
        .byte 31
        .byte 30
        .byte 31

wrong_argc:
        movq $usage_format, %rdi
        movq (%rsi), %rsi # %rsi still hold argv up to this point
        call printf

failed:
        movq $1, %rax

        movq %rbp, %rsp
        popq %rbp
        ret

.data
menu_win:       .fill 1, 8
menu_sub:       .fill 1, 8
played_cards:   .fill 1, 8
dealer_win:     .fill 1, 8
input_win:      .fill 1, 8
linecount:      .fill 1, 8, 1
maxlinecount:   .fill 1, 8
inputcount:     .fill 1, 8, 1
user_input_buffer:       .fill 64, 8


## MAX_COLUMNS: .fill 1
## MAX_ROWS:    .fill 1
